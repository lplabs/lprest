var LP = require("../index.js");
var secrets = require("./adhoc-secret.json");
var util = new require("util");


var lp = new LP({
//    version: "1", //optional
    signed: secrets.api
});
console.log("Getting Account");
lp.account("P9700259", secrets.P9700259).then(function (account) {
    console.log("Got Account");
    return account.chatAvailability().then(function (availability) {
        if (!availability) {
            throw new Error("No available agents, aborting");
        }
    }).then(function () {
            console.log("-------------------------\n\n\n");
            return account.chatRequest({
                skill: "LiveEngage",
                customVariables: {
                    customVariable: {
                        name: "identifier",
                        value: "test"
                    }
                },
                preChatLines: {
                    line: "A PRE CHAT LINE"
                },
                userAgent: "Mobile_Device",
                chatReferrer: "SMS"
            })
        }).then(function (chat) {
            var cl = console.log.bind(cl);

            ["info", "state","line","url","a2a-transfer"].forEach(function(eventType) {
                chat.on(eventType, function(data) {
                    console.log(eventType, "event:",data);
                })
            });

            return chat.init(); //loading info, links
        }).then(function(chat) {
            console.log("Chat Loaded");

//            chat.transcriptRequest("asafh@liveperson.com").done(function() {
//                console.log("Transcript Requested");
//            });

//            chat.customVariables({"Test Param": "Test Value"}).done(function() {
//                console.log("Custom Variables set!");
//            });

//            require("../node_modules/hypermed/node_modules/request").debug = true;

            chat.events().done(function(events) {
                console.log("Got events", events.length);
            });

            var altChat = lp.chat(chat.location(), secrets.P9700259).init();
            altChat.then(function(chat) {
                chat.sendLine("IT'S STILL ME FROM ANOTHER PLACE");
                console.log("Second Chat Reference Loaded!!!");
                var poller = chat.polling(1000);
                poller.done(function() {
                    console.log("SECOND Polling Stopped!");
                }, function(err) {
                    console.log("SECOND An error occurred while polling",err);
                    throw err;
                }, function(events) {
                    console.log(events.length, "SECOND new events!");
                });
                setTimeout(poller.stop, 5000);
            });


            var poller = chat.polling(1000);
            poller.done(function() {
                console.log("Polling Stopped!");
            }, function(err) {
                console.log("An error occurred while polling",err);
                throw err;
            }, function(events) {
                console.log(events.length, "new events!");
            });
            setTimeout(poller.stop, 5000);

            //Not working yet:
//            chat.exitSurvey().done(function(survey) {
//                console.log("Exit Survey", survey);
//            });



        });
//    console.log(account);
},function (err) {
    console.log("Error", err);
}).done();

var Chat = new require("./Chat.js");
var account = new require("./account.js");
var LPHypermed = new require("./LPHypermed.js");

var LP = function LP(options) {
    var lp = this;
    var hyper = this._hyper = LPHypermed(options);
    this.chat = function(location, token) {
        return new Chat(hyper.path(location, {
            token: token
        }));
    };
    this.account = function (site, token) {
        return account(hyper.path(site, {
            token: token
        }));
    };
};

LP.Chat = Chat;


module.exports = LP;


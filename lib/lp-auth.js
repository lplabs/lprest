var OAuth = require('oauth-1.0a');
var url = require('url');
var _ = require('lodash');

function isRedirect(response) {
    return response.statusCode >= 300 && response.statusCode < 400;
}


module.exports = function (api_keys) {
    return function (req, options, next, hypermed) {
        var redirectFilter;
        if (req.followRedirect) {
            redirectFilter = typeof req.followRedirect === "function" ? req.followRedirect : function () {
                return true;
            };
            req.followRedirect = false;
        }
        var oauth = OAuth({
            consumer: {
                public: api_keys.public,
                secret: api_keys.secret
            },
            signature_method: 'HMAC-SHA1'
        });
        req.url = url.format(req.url); ///The oauth library doesn't support url objects.
//        console.log("Signing", req);
        _.extend(req.headers, oauth.toHeader(oauth.authorize(req, options.token)));

        if (!redirectFilter) {
            return next();
        }
        return next().then(function (values) {
            var response = values[0];
            if (!isRedirect(response)) {
                return values;
            }
            var newOptions = redirectFilter(response);
            if (newOptions === true) { //just redirect, no speical options
                newOptions = {
                    url: response.headers.location,
                    method: "GET"
                };
            }

            return hypermed.send(newOptions);
        });
    };
};
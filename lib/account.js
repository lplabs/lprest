var Chat = new require("./Chat.js");

function filterParameters(names, params) {
    var ret = {};
    if (params) {
        names.forEach(function (name) {
            if (params.hasOwnProperty(name)) {
                ret[name] = params[name];
            }
        });
    }
    return ret;
}

var account = function account(accessor) {
    return accessor.get({}, {followRedirect: true}).then(function (account) {
        var val = {
            id: account.id
        };
        var accessors = {};
        accessors["chat-availability"] = account.link("chat-availability", {
            entityKey: false
        });
        val.chatAvailability = function (params) {
            var params = filterParameters(["skill", "agent", "serviceQueue", "maxWaitTime"], params);
            return accessors["chat-availability"].get(params).then(function (availObj) {
                return availObj.availability;
            });
        };

        accessors["chat-request"] = account.link("chat-request", {method: "POST"});

        /**
         * request is the json body https://community.liveperson.com/docs/DOC-1044#jive_content_id_Chat_Request
         * @param request
         * @returns {*}
         */
        val.chatRequest = function (request) {
            return accessors["chat-request"].create({request: request || {}})
                .then(function (location) {
                    return new Chat(accessor.path(location));
                });
        };
        return val;
    });
};

module.exports = account;
var Hypermed = require("./../../hypermed");
var lpauth = new require("../lib/lp-auth.js");
var _ = new require("lodash");

/**
 *
 * @param options v - version, signed - lpauth data.
 * @returns {*}
 */
var LPHypermed = module.exports = function LPHypermed(options) {
    var hyper = new Hypermed(_.extend({
        entityKey: true,
        followRedirect: false,
        "url": "https://api.liveperson.net/api/account/"
    }, options.hypermed || {}))
        .use(new Hypermed.ParamsFilter({
            v: options.version || "1"
        }, true))
        .use(new Hypermed.MethodOverrides({
            "PUT": "POST"
        }))
    if (options.signed) {
        hyper.use(new lpauth(options.signed));
    }
    return hyper;
}
